<div align="justify">

## Generating the results


* #### Producing the Promoter architecture related features:
You can use 'promoter_architecture.R' to produce the Promoter architecture related features for the arbitrary tissue. It is notable that this code also contains all plots and sanity checks that we used to produce the final data. 

You can use 'share.R' to produce the results for all tissues. Actually, 'share.R' is based on a summarization of the 'promoter_architecture.R' file, which is explained below.


## Promoter Shape

CAGE data for 31 tissues (library size of about 10M mapped reads or above, Supplementary table 11) was downloaded from FANTOM5 project (Lizio et al. 2015) using CAGEr package in R (Haberle et al. 2015). On each dataset separately, we did power-law normalization (Balwierz et al. 2009) using CAGEr package. TSSs with low count numbers (less than 5 counts) were removed. Next, we applied a simple clustering method (distclu, maximum distance = 20) form CAGEr package on each dataset separately. Clusters with low normalized CAGE signals (sum of TSSs normalized signals of the cluster below 10-50 depending on the tissue) were removed. CAGE clusters were then assigned to genes by overlapping them with core promoter regions (-300/+200 bp around TSSs of all annotated transcripts). Clusters that did not overlap any core promoters were removed.

Next we defined promoter shape for all CAGE clusters by using two commonly accepted metrics: promoter width and promoter shape index. Promoter width was calculated by using the inter-percentile width of 0.05 and 0.95 following methodology from (Haberle et al. 2015). Promoter shape index (SI) was calculated by the formula as above (Drosophila section) proposed in (Hoskins et al. 2011). 

For classifying promoters into broad and narrow based on promoter width, we used the following approach. First, we did a linear transformation of promoter width values (actual value minus 1 divided by 10; for fitting gamma distribution) On the transformed data, we fitted gamma mixture model (2 gamma distribution), and parameters was trained using EM algorithm (Dempster, Laird, and Rubin 1979) using mixtools package in R (Benaglia et al. 2009). The threshold for classifying promoters as broad or narrow was selected by finding the point which best separates the two distributions. Following this approach, promoters with width above about 10-15 bp. were classified as broad, which was consistent across all tissues and agreed with earlier studies (Forrest et al. 2014). To classify promoters into broad/narrow using shape index, we fitted Gaussian mixture model (2 Gaussian distribution) to the data and selected the threshold separating the two distributions using the same approach as above. For the subsequent analysis, we used promoter width feature since it showed more clear bi-modal distribution in all tissues (example in Supplementary fig 7a-c) and is a more common metrics in the analysis of mammalian promoters (Forrest et al. 2014; Carninci et al. 2006).  

Each gene was then assigned the promoter width of its main transcript. If more than one CAGE cluster was present for a gene’s main transcript, the cluster with the highest normalized CAGE signal was selected. Promoter width values for most of the genes were highly correlated across tissues (Supplemetary fig 7d). Based on the tissue-specific shape data, we calculated two aggregated features for each gene. Mean promoter width (mean_width feature) was calculated as the mean of gene promoter widths in all tissues where it had CAGE signal (passing the filtering criteria defined above). Share of tissues where a gene had broad promoter (percentage_of_broad feature) was calculated for each gene by dividing the number of tissues where the gene had broad promoter by the total number of tissues where the gene had CAGE signal.

</div>