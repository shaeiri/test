
# Essential libs.

library(CAGEr)
library(rtracklayer)
library(GenomicFeatures)
library(GenomicRanges)
library(plyr)
library(dplyr)
library(entropy)
library(SDMTools)
library(BBmisc)
library(mixtools)


# Reading data.

data("FANTOM5humanSamples")

samples <- c("occipital_pole__adult__pool1", "ovary__adult__pool1", "paracentral_gyrus__adult__pool1"
	     , "parietal_lobe__adult__pool1", "placenta__adult__pool1", "pons__adult__pool1"
             , "postcentral_gyrus__adult__pool1", "prostate__adult__pool1", "retina__adult__pool1"
             , "skeletal_muscle__adult__pool1", "small_intestine__adult__pool1", "temporal_lobe__adult__pool1"
             , "testis__adult__pool1", "thymus__adult__pool1", "thyroid__adult__pool1"
             , "tonsil__adult__pool1", "trachea__adult__pool1", "bladder__adult__pool1"
	     , "blood__adult__pool1", "brain__adult__pool1", "cervix__adult__pool1"
             , "colon__adult__pool1", "corpus_callosum__adult__pool1", "esophagus__adult__pool1"
             , "frontal_lobe__adult__pool1", "insula__adult__pool1", "kidney__adult__pool1"
             , "liver__adult__pool1", "lung__adult__pool1", "medulla_oblongata__adult__pool1"
             , "nucleus_accumbens__adult__pool1")


ce <- importPublicData(source = "FANTOM5", dataset = "human"
                       , sample = samples)


# Making a refrence model.
location <- "/g/scb2/zaugg/shaeiri/project_data/ensembl_hg19"
txdb <- makeTxDbFromGFF(file = location, format = "gtf")

upstream <- 300
downstream <- 200

select_col <- c("TXNAME")

Pr_region <- promoters(txdb, upstream = upstream, downstream = downstream
                       , columns = select_col, use.names = FALSE)
Pr_region_trim <- trim(Pr_region)

intervals <- sort(Pr_region_trim, by = ~ seqnames + start + end)


# Analysis.
plotReverseCumulatives(ce, fitInRange = c(5, 30000))

normalizeTagCount(ce, method = "powerLaw", fitInRange = c(5, 30000)
                  , alpha = 1.20, T = 10 ^ 7)

threshold <- 5


maxDist <- 20

clusterCTSS(object = ce
            , threshold = threshold
            , thresholdIsTpm = FALSE
            , nrPassThreshold = 1
            , removeSingletons = FALSE
            , method = "distclu"
            , maxDist = maxDist
            , useMulticore = TRUE)

cumulativeCTSSdistribution(ce, clusters = "tagClusters", useMulticore = TRUE)

qup <- 0.95
qlow <- 0.05
quantilePositions(ce, clusters = "tagClusters", qLow = qlow, qUp = qup)


# Loop.

for (i in seq(2, length(samples))) {
  
  print(i)
  
  currentSample <- sampleLabels(ce)[i]
  
  # Choosing a good TH.
  TCs <- tagClustersGR(ce, sample = currentSample)
  
  seq <- unique(TCs$score)
  n <- c()
  for (i in seq)
    n <- c(n, length(TCs[TCs$tpm >= i]))
  
  temp <- cbind.data.frame(seq, n)
  temp <- temp[order(temp$seq), ]
  max <- 0
  index <- -1
  for (i in seq(2, nrow(temp) - 1)){
    d <- min(temp$seq[i] - temp$seq[i - 1], temp$seq[i + 1] - temp$seq[i]) ^2
    value <- abs(temp$n[i - 1] + temp$n[i + 1] - 2 * temp$n[i]) / d
    if (value > max) {
      max <- value
      index <- i
    }
  }
  
  TH <- temp$seq[index]
  print(TH)
  
  # Filttering.
  TCs <- tagClustersGR(ce, sample = currentSample
                       , returnInterquantileWidth = TRUE
                       , qLow = qlow, qUp = qup)
  
  TCs <- TCs[TCs$tpm > TH]
  TCs <- TCs[width(TCs) != 1]
  
  TCs <- sort(TCs, by = ~ seqnames + start + end)
  
  # Method1.
  selection <- findOverlaps(TCs, intervals)
  
  df1 <- as.data.frame(TCs)[queryHits(selection), ]
  df2 <- as.data.frame(intervals)[subjectHits(selection), ]
  
  main <- cbind(df1, df2)
  for (i in seq(length(colnames(df1)) + 1, length(colnames(df1)) + length(colnames(df2))))
    colnames(main)[i] <- paste(colnames(main)[i], "2", sep = "")
  
  TCs <- TCs[unique(queryHits(selection))]
  TCs <- sort(TCs, by = ~ seqnames + start + end)
  
  
  # Computing promoter widths.
  width <- data.frame(TCs$cluster, TCs$interquantile_width)
  width <- width[with(width, order(TCs$cluster)), ]
  s0 <- width$TCs.interquantile_width
  
  # cutoff.
  temp_width <- (s0 - min(s0)) / 10
  
  alpha <- c(1, 3.5)
  beta <- c(2, 1)
  mixmd1 <- gammamixEM(temp_width, lambda = c(0.5, 0.5), alpha = alpha, beta = beta, k = 2
                       , epsilon = 1e-10, maxit = 1000, maxrestarts = 20, verb = FALSE)
  
  start <- 0
  end <- 5
  step <- 0.1
  
  min = 100
  index <- 0
  for (i in seq(start, end, step)) {
    temp <- mixmd1$lambda[1] * dgamma(i, mixmd1$gamma.pars[1], mixmd1$gamma.pars[2]) - 
      mixmd1$lambda[2] * dgamma(i, mixmd1$gamma.pars[3], mixmd1$gamma.pars[4])
    temp <- abs(temp)
    if (min > temp) {
      min <- temp
      index <- i
    }
  }
  cutoff1 <- index
  print(cutoff1)
  
  
  # SI.
  data <- CTSSnormalizedTpmGR(ce, samples = currentSample)
  
  data <- data[data$filteredCTSSidx == TRUE]
  
  data <- sort(data, by = ~ seqnames + pos)
  
  
  ol <- findOverlaps(TCs, data)
  
  df1 <- as.data.frame(TCs)[queryHits(ol), ]
  df2 <- as.data.frame(data)[subjectHits(ol), ]
  
  df <- cbind(df1, df2)
  for (i in seq(length(colnames(df1)) + 1, length(colnames(df1)) + length(colnames(df2))))
    colnames(df)[i] <- paste(colnames(df)[i], "2", sep = "")
  head(df, 10)
  
  si <- df %>%
    group_by(cluster) %>%
    summarize(SI = 2 + -1 * entropy.empirical(score2, unit="log2"))
  si <- as.data.frame(si)
  
  s4 <- si$SI
  
  
  # Making results table.
  results <- cbind.data.frame(cluster = width$TCs.cluster
                              , width = width$TCs.interquantile_width
                              , si = si$SI)
  
  results <- results %>%
    mutate(type1 = case_when (
      results$width <= (cutoff1 * 10) + min(s0)  ~ 0,
      results$width > (cutoff1 * 10) + min(s0) ~ 1))
  
  table <- main[, c("cluster", "tpm", "TXNAME2")]
  names(table)[3] <- "TXNAME"
  table1 <- join(table, results, by = "cluster", type = "inner", match = "all")
  
  final <- table1 %>%
    group_by(TXNAME) %>%
    summarize(width = width[which.max(tpm)]
              , si = si[which.max(tpm)]
              , type1 = type1[which.max(tpm)])
  final <- as.data.frame(final)
  
  if (i == 1) {
    TABLE <- final
   } else {
    TABLE <- merge(TABLE, final, by = "TXNAME", all = TRUE)
   }
  
  TABLE <- merge(TABLE, final, by = "TXNAME", all = TRUE)
}


# Finalizing.

matrix <- as.matrix(TABLE[, c(-1)])

matrix1 <- matrix[, seq(1, 32 * 3, 3)]
matrix2 <- matrix[, seq(2, 32 * 3, 3)]
matrix3 <- matrix[, seq(3, 32 * 3, 3)]


sum <- rowSums(matrix3, na.rm = TRUE)
TABLE$share_broad <- sum

na <- rowSums(is.na(matrix3))
na <- 32 - na
TABLE$percent_broad <- TABLE$share_broad / na


m1 <- rowMedians(matrix1, na.rm = TRUE)
m2 <- rowMeans(matrix1, na.rm = TRUE)
TABLE$median_width <- m1
TABLE$mean_width <- m2


m3 <- rowMedians(matrix2, na.rm = TRUE)
m4 <- rowMeans(matrix2, na.rm = TRUE)
TABLE$median_si <- m3
TABLE$mean_si <- m4


# Saving.
l1 <- "/g/scb2/zaugg/shaeiri/data/shapes.csv"
write.csv(TABLE, l1, row.names = FALSE)

l2 <- "/g/scb2/zaugg/shaeiri/data/share_shape.csv"
write.csv(TABLE[, c(1, 98, 99, 100, 101, 102, 103)], l2, row.names = FALSE)
