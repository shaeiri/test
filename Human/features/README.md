<div align="justify">

## Generating the results

* #### Downloading the reference model:
You can download the reference model from the [UCSC table browser](https://genome.ucsc.edu/cgi-bin/hgTables). We used the ensemble version of hg19, but also our results are robust and they are not depending on the specific version.

* #### Producing different features:
You can check the corresponding folder for each feature to produce that feature.


## Genome version

We used Ensembl GRCH37/hg19 genome version downloaded from UCSC table browser (Kent et al. 2002; Haeussler et al. 2019) throughout the analysis.


## Feature tables for human dataset
Only TSS-proximal features and several gene properties (i.e. gene length and number of transcripts) were used to predict expression level and variation in human. Full list of features used in this analysis is provided in Supplementary table 10. Most of the TSS-proximal features (TF peaks and chromatin states) were scanned in the −500/+500 bp of the main TSS of the genes (referred to as TSS-proximal regions), following the same approach as for Drosophila. Several features more strictly linked to the gene core promoters (promoter shape, TATA-box, CpG islands, and promoter GC-content) were scanned in -300/+200 bp of the main TSS of the genes (referred to as core promoter regions).

</div>