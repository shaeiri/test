
## Essential libraries.
library(GenomicFeatures)
library(dplyr)


## Computing features that related to exons.

# Reading refrence model.
location1 <- "/Users/shaeiri/Desktop/Homo_sapiens.GRCh37.75.gtf"
txdb <- makeTxDbFromGFF(file = location1, format = "gtf")

columns(txdb)

# Computing.
select_col <- c("GENEID")
exon_region <- exons(txdb, columns = select_col, use.names = FALSE)
exon_region <- sort(exon_region, by = ~ seqnames + start + end)

df <- as.data.frame(exon_region)
df$GENEID <- as.character(df$GENEID)

table <- df %>%
  group_by(GENEID) %>%
  summarize(mean_exons = mean(width)
            , dispersion_exons = sum(abs(width - mean(width))) / n()
            , number_exons = n())


## Saving.
location2 <- "/Users/shaeiri/Desktop/exons.csv"
write.csv(table, location2, row.names = FALSE)
