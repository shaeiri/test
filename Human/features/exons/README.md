<div align="justify">

## Reproducing the results

* #### Producing exons related features:
You can use 'exons.R' to produce the related fatures.


## Gene properties

Number of transcripts, gene length, mean exon length, number of exons and exon length mean absolute deviation were calculated for each gene directly using hg19 genome annotation from Ensembl website (Cunningham et al. 2019). Transcripts width was calculated for each transcript by using the same file, and length of the main transcript was assigned to each gene.

</div>