<div align="justify">

## Generating the results

* #### Downloading the raw data:
You can download the raw data from the [UCSC Genome Browser](https://genome.ucsc.edu/).

* #### Producing CpG islands related features:
You can use 'CpG_islands.R' to produce the related fatures.


## CpG Islands

CpG islands (CGI) data for hg19 were downloaded from the UCSC Genome browser (Haeussler et al. 2019). For each CGI, these included CGI length (CpG_Length), number of CpG clusters (CpGNum) and number of GC dinucleotides (gcNum). The three corresponding features for each gene were calculated by overlapping CGI regions and gene core promoter regions (-300/+200 bp). When a gene did not overlap any CGI, the three features were assigned to 0. If multiple overlaps were present, CGI with the biggest overlap was considered for each gene.

</div>