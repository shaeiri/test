<div align="justify">

## Generating the results

* #### Producing GC contents related features:
You can use 'gccontent.R' to produce the related fatures.


## Promoter GC-Content
Promoter GC-content (GC_content) was calculated by using biostring package (Pagès H et al 2019) and BSgenome.Hsapiens.UCSC.hg19 v1.4.0 in R in gene core promoter regions (-300/+200 bp).

</div>