#!/bin/bash

#SBATCH -J variation
#SBATCH -A Zaugg
#SBATCH -N 1
#SBATCH -n 24
#SBATCH --mem=30000
#SBATCH -t 1-00:00:00
#SBATCH -o /g/scb2/zaugg/shaeiri/outs2/%N.%j.out
#SBATCH -e /g/scb2/zaugg/shaeiri/outs2/%N.%j.err

module add R/3.5.1-foss-2017b-X11-20171023
module add R-bundle-Bioconductor-GBCS/3.7-foss-2017b-R-3.5.1
Rscript /g/scb2/zaugg/shaeiri/variation_prediction/table/target/src/job4.R $1