#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
print(args[1])


### Essential libraries.
library(data.table)
library(CePa)
library(plyr)
library(dplyr)
library(stringr)
library(DESeq2)


### Reading data.

## Reading SampleAttributes.
loc1 <- "/g/scb2/zaugg/shaeiri/project_data/GTEx_v7_Annotations_SampleAttributesDS.txt"
col1 <- c("character", "character", "character", "factor", "float64")
names(col1) <- c("SAMPID", "SMTS", "SMTSD", "SMATSSCR", "SMTSISCH")
table1 <- fread(loc1, header = TRUE, sep = "\t", colClasses = col1, select = names(col1))

temp <- table1 %>%
  mutate(Ischemic = case_when (
    table1$SMTSISCH <= 300 ~ "1-300",
    table1$SMTSISCH > 300 & table1$SMTSISCH <= 600  ~ "301-600",
    table1$SMTSISCH > 600 & table1$SMTSISCH <= 900  ~ "601-900",
    table1$SMTSISCH > 900 & table1$SMTSISCH <= 1200 ~ "901-1200",
    table1$SMTSISCH > 1200 ~ "1200-"))
temp <- as.data.frame(temp)
table1$Ischemic <- as.factor(temp$Ischemic)

temp <- c()
for (i in seq(nrow(table1))) {
  temp <- c(temp, substring(table1$SAMPID[i], 1
                            , str_locate_all(table1$SAMPID[i], "-")[[1]][2, 1] - 1))
}
table1$SUBJID <- temp

## Reading SubjectPhenotypes.
loc2 <- "/g/scb2/zaugg/shaeiri/project_data/GTEx_v7_Annotations_SubjectPhenotypesDS.txt"
col2 <- c("charachter", "factor", "factor", "factor")
names(col2) <- c("SUBJID", "AGE", "SEX", "DTHHRDY")
table2 <- fread(loc2, header = TRUE, sep = "\t", colClasses = col2, select = names(col2))

## Joining these two.
table_main <- join(table1, table2, by = "SUBJID", type = "inner")

## Our groups.
groups <- c("AGE", "SEX", "DTHHRDY", "SMATSSCR", "Ischemic")

## Reading counts.
location1 <- "/g/scb2/zaugg/shaeiri/project_data/All_Tissue_Site_Details.combined.reads.gct"
data <- read.gct(location1)
print("finish")

## Tissues.
loc3 <- "/g/scb2/zaugg/shaeiri/project_data/GTEx Portal.csv"
table3 <- fread(loc3, header = TRUE, sep = ",")


### Main Function.

cut2 <- 100
counter <- as.integer(args[1])

tissue <- table3$Tissue[counter]
table <- table_main[table_main$SMTSD == tissue, ]

## Selecting counts of the tissue.
temp <- as.data.frame(colnames(data))
names(temp)[1] <- "SAMPID1"
temp$SAMPID1 <- as.character(temp$SAMPID1)
temp$SAMPID <-  gsub("[.]", "-", temp$SAMPID1)

table <- join(temp, table, by = "SAMPID", type = "inner")

data1 <- data[, table$SAMPID1]

## Checking number of samples.
if (length(table$SAMPID) <= cut2)
  stop("END.")

## Filtering some rows.
sum <- rowSums(data1)
data1 <- data1[sum > 20, ]
medians <- rowMedians(data1)
data1 <- data1[medians != 0, ]
data2 <- data1

## Info.
print(tissue)
print(ncol(data1))
print(nrow(data1))

## Making DESeq2 object again with final data.
dds <- DESeqDataSetFromMatrix(countData = data2, colData = table, design = ~ 1)

featureData <- data.frame(gene = rownames(data2))
mcols(dds) <- DataFrame(mcols(dds), featureData)

## Normalization.
dds <- estimateSizeFactors(dds)
data2 <- counts(dds, normalized = TRUE)

## Computing variation of genes with different metrics.

mean <- rowMeans(data2)
mean <- log(mean)

median <- rowMedians(data2)
median <- log(median)

variance <- rowVars(data2)
variance <- log(variance)

sd <- sqrt(rowVars(data2))
sd <- log(sd)

cv <- sqrt(rowVars(data2)) / rowMeans(data2)
cv <- log(cv)

my_data <- cbind.data.frame(median, mean, cv)

# Removing outliers.
my_data <- my_data[my_data$median < quantile(my_data$median, 0.95) & 
                   my_data$median > 5, ]

# Residuals.
span2 <- 0.25
degree2 <- 1
model2 <- loess(cv ~ median, data = my_data, degree = degree2, span = span2)
r2 <- resid(model2)

## Making ID column.
tt <- data2[median < quantile(median, 0.95) & 
            median > 5, ]

gene_id <- gsub("\\.\\d+$", "", rownames(tt))

# gene_id <- gsub("\\.\\d+$", "", rownames(data2))


## Final tables.
res1 <- cbind.data.frame(gene_id = gene_id
                         , mean = my_data$mean
                         , median = my_data$median
                         , cv = my_data$cv
                         , residual_cv = r2)
for (i in seq(2, ncol(res1)))
  names(res1)[i] <- paste(tissue, "_", names(res1)[i], sep="")


### Saving.
location2 <- paste("/g/scb2/zaugg/shaeiri/variations/variation_v6_", table3$Tissue[as.integer(args[1])], ".csv", sep="")
write.csv(res1, location2, row.names = FALSE)

