suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ranger))
suppressPackageStartupMessages(library(mlr))
suppressPackageStartupMessages(library(Boruta))
options(stringsAsFactors = FALSE)

source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir
outdir = file.path(project_folder, "/analysis/random_forests")
make_recursive_dir(outdir)

human_path1 = file.path(project_folder, "/analysis/human_data")
human_path2 = "/g/scb2/zaugg/shaeiri/variation_prediction/table/final/"

# Boruta results
features = read.csv(file.path(human_path1, "Boruta_median_importance_results.csv"))

# tables
mt_av_var = read.csv(file.path(human_path2, "share.csv")) %>% rename(resid_cv = mean_variation, median = mean_median)
mt_prior = read.csv(file.path(human_path2, "prior.csv"))
mt_lung = read.csv(file.path(human_path2, "Lung.csv"))
mt_muscle = read.csv(file.path(human_path2, "Muscle.csv"))
mt_ovary = read.csv(file.path(human_path2, "Ovary.csv"))

# remove some columns
#remove_list = names(mt)[grep("_cv|_median|variation|_mean|Prior_Rank|gene_name|gene_id", names(mt))]



# full dataset, predict variation
#mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE)

######## RF full ###################################################

mt1 = mt_av_var %>% select(resid_cv, one_of(features$feature[features$tissue == "average" & features$responce == "variation"])) 
mt2 = mt_av_var %>% select(median, one_of(features$feature[features$tissue == "average" & features$responce == "level"]))
mt3 = mt_prior %>% select(DE_Prior_Rank, one_of(features$feature[features$tissue == "" & features$responce == "DE prior"]))

mt4 = mt_lung %>% select(resid_cv, one_of(features$feature[features$tissue == "lung" & features$responce == "variation"])) 
mt5 = mt_lung %>% select(median, one_of(features$feature[features$tissue == "lung" & features$responce == "level"])) 
mt6 = mt_muscle %>% select(resid_cv, one_of(features$feature[features$tissue == "muscle" & features$responce == "variation"])) 
mt7 = mt_muscle %>% select(median, one_of(features$feature[features$tissue == "muscle" & features$responce == "level"]))
mt8 = mt_ovary %>% select(resid_cv, one_of(features$feature[features$tissue == "ovary" & features$responce == "variation"])) 
mt9 = mt_ovary %>% select(median, one_of(features$feature[features$tissue == "ovary" & features$responce == "level"]))


lrn = makeLearner("regr.ranger")

task1 = makeRegrTask(data = mt1, target = "resid_cv")
task2 = makeRegrTask(data = mt2, target = "median")
task3 = makeRegrTask(data = mt3, target = "DE_Prior_Rank")
task4 = makeRegrTask(data = mt4, target = "resid_cv")
task5 = makeRegrTask(data = mt5, target = "median")
task6 = makeRegrTask(data = mt6, target = "resid_cv")
task7 = makeRegrTask(data = mt7, target = "median")
task8 = makeRegrTask(data = mt8, target = "resid_cv")
task9 = makeRegrTask(data = mt9, target = "median")

rdesc.outer = makeResampleDesc("CV", iters = 5)
ms = list(rsq, rmse, mse)

bmr_full = benchmark(lrn, tasks = list(task1, task2, task3, task4, task5, task6, task7, task8, task9), resampling = rdesc.outer, measures = ms, show.info = TRUE)

df_full = data.frame(bmr_full)
df_full$response = c(rep(c("variation", "level", "DE prior", "variation", "level", "variation", "level", "variation", "level"), each = 5))
df_full$type = rep(c("average", "average", "", "lung", "lung", "muscle", "muscle", "ovary", "ovary"), each = 5)

write.csv(df_full, file.path(human_path1, "Random_forest_results.csv"), row.names = FALSE)
