suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ranger))
suppressPackageStartupMessages(library(mlr))
suppressPackageStartupMessages(library(Boruta))
options(stringsAsFactors = FALSE)

source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
project_folder = config$project_parameters$project_dir
# master_table = load_master_table()
# mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE)
outdir = file.path(project_folder, "/analysis/random_forests")


######################################################################
######## RF full #####################################################
######################################################################

human_path = file.path(project_folder, "/analysis/human_data")
boruta_path = "/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta_human/"

################################### Mean variation and prior #######################################

mt = read.csv(file.path(human_path, "variation_prior.csv"))
mt_lung = read.csv(file.path(human_path, "mt_lung.csv"))

######## Boruta results ###################################################


boruta_var = readRDS(file.path(boruta_path, "predict_mean_variation_glob/results/1.rds"))
imp_var = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_mean_level/results/1.rds"))
imp_lev = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_DE_Prior_Rank/results/1.rds"))
imp_prior = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_resid_cv_lung/results/1.rds"))
imp_var_lung = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_median_lung/results/1.rds"))
imp_lev_lung = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_resid_cv_ovary/results/1.rds"))
imp_var_ov = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_median_ovary/results/1.rds"))
imp_lev_ov = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_resid_cv_muscle/results/1.rds"))
imp_var_mus = boruta_get_importance(boruta_var)

boruta_var = readRDS(file.path(boruta_path, "predict_median_muscle/results/1.rds"))
imp_lev_mus = boruta_get_importance(boruta_var)


data.frame(var = names(imp_var), med_imp = imp_var)
data.frame(var = names(imp_lev), med_imp = imp_lev)
data.frame(var = names(imp_prior), med_imp = imp_prior)
data.frame(var = names(imp_var_lung), med_imp = imp_var_lung)
data.frame(var = names(imp_var_lung), med_imp = imp_var_lung)


# importance for variation
boruta_var = readRDS(config$feature_selection$resid_cv)
imp_var = boruta_get_importance(boruta_var)
imp_var = data.frame(var = names(imp_var), med_imp_var = imp_var)

# importance for level
boruta_med = readRDS(config$feature_selection$median)
imp_med = boruta_get_importance(boruta_med)
imp_med = data.frame(var = names(imp_med), med_imp_med = imp_med)

# importance for shape
boruta_shape = readRDS(config$feature_selection$shape)
imp_shape = boruta_get_importance(boruta_shape)
imp_shape = data.frame(var = names(imp_shape), med_imp_shape = imp_shape)

# importance for variation in narrow
boruta_var = readRDS(config$feature_selection$resid_cv_narrow)
imp_var_narrow = boruta_get_importance(boruta_var)
imp_var_narrow = data.frame(var = names(imp_var_narrow), med_imp_narrow_var = imp_var_narrow) 

# # importance for variation in broad
boruta_var = readRDS(config$feature_selection$resid_cv_broad)
imp_var_broad = boruta_get_importance(boruta_var)
imp_var_broad = data.frame(var = names(imp_var_broad), med_imp_broad_var = imp_var_broad) 

# importance for level in narrow
boruta_var = readRDS(config$feature_selection$median_narrow)
imp_med_narrow = boruta_get_importance(boruta_var)
imp_med_narrow = data.frame(var = names(imp_med_narrow), med_imp_narrow_lev = imp_med_narrow) 

# # importance for level in broad
boruta_var = readRDS(config$feature_selection$median_broad)
imp_med_broad = boruta_get_importance(boruta_var)
imp_med_broad = data.frame(var = names(imp_med_broad), med_imp_broad_lev = imp_med_broad) 

boruta_imp = merge_df_list(list(imp_var, imp_med, imp_shape, imp_var_broad, imp_var_narrow, imp_med_narrow, imp_med_broad), merge_by = "var", all_both = TRUE)
#boruta_imp[is.na(boruta_imp)] = 0.1 # pseudocount for non-signifcinat features 


######## Correlations with response ########################################

#all factors must be converted to numeric or logical

# check condition
# boruta_imp$va[!unlist(lapply(boruta_imp$var, function(x) is.numeric(mt[,x]) | is.logical(mt[,x]) ))]

mt$conserv_rank = as.numeric(mt$conserv_rank)
mt$shape = as.numeric(mt$shape)
mt$dhs_time_profile.prox = as.numeric(mt$dhs_time_profile.prox)
mt$dhs_tissue_profile.prox = as.numeric(mt$dhs_tissue_profile.prox)
mt$dhs_time_profile.dist = as.numeric(mt$dhs_time_profile.dist)
mt$dhs_tissue_profile.dist = as.numeric(mt$dhs_tissue_profile.dist)
mt[, grep("modERN_cisbp", names(mt))] = apply(mt[, grep("modERN_cisbp", names(mt))], 2, as.numeric)


# correlations
cor1 = get_correl_for_feature_list(mt, "resid_cv", boruta_imp$var) %>% rename(cor_var = cor)
cor2 = get_correl_for_feature_list(mt, "median", boruta_imp$var) %>% rename(cor_med = cor)
cor3 = get_correl_for_feature_list(mt, "shape", boruta_imp$var) %>% rename(cor_shape = cor)
cor4 = get_correl_for_feature_list(mt, "major_shape_ind", boruta_imp$var) %>% rename(cor_shape_ind = cor)
cor5 = get_correl_for_feature_list(mt %>% filter(shape == 1), "resid_cv", boruta_imp$var) %>% rename(cor_var_broad = cor)
cor6 = get_correl_for_feature_list(mt %>% filter(shape == 2), "resid_cv", boruta_imp$var) %>% rename(cor_var_narrow = cor)
cor7 = get_correl_for_feature_list(mt %>% filter(shape == 1), "median", boruta_imp$var) %>% rename(cor_med_broad = cor)
cor8 = get_correl_for_feature_list(mt %>% filter(shape == 2), "median", boruta_imp$var) %>% rename(cor_med_narrow = cor)


cor = merge_df_list(list(cor1, cor2, cor3, cor4, cor5, cor6, cor7, cor8), all_both = TRUE, merge_by = "var")

######## Correlations among features #################################

tmp = mt[ , cor$var]
cor_features = data.frame(cor(tmp))
cor_features$var = row.names(cor_features)
cor_features %<>% gather(high_cor_feature, cor_with_feature, -var)

# find features with correlation > 0.9, in our case - only pairs
top_cor = cor_features %>% 
  filter(var != high_cor_feature & abs(cor_with_feature) > 0.9)


cor = merge(cor, top_cor, by = "var", all.x = TRUE)


######## combine correlations and importance ##########################

df = merge(boruta_imp, cor, by = "var") 


######## Add feature classes ##########################################

path = config$processed_features$feature_classes
feature_classes = read.csv(path)

df = merge(feature_classes, df, by.y = "var", by.x = "feature", all.y = TRUE) %>% arrange(-med_imp_var) 


######## Add corrected names ##########################################
features_info = read.csv(config$processed_features$features_info) %>% select(var, full_name)
df = merge(features_info, df, by.x = "var", by.y = "feature") %>% arrange(-med_imp_var) 

#write.csv(df %>% filter(!is.na(med_imp_var)), file.path(outdir, "significant_features_importance_and_correlations.csv"), row.names = FALSE)
write.csv(df, file.path(outdir, "significant_features_importance_and_correlations.csv"), row.names = FALSE)
