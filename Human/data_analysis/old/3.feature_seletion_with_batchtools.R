library(batchtools)
library(checkmate)
library(randomForest)
library(ranger)
library(Boruta)
library(tidyverse)
library(magrittr)
#library(knitr)
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
batchtools = config$software$batchtools_functions
source(batchtools)

#times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir 
boruta_base_dir = file.path(project_folder, "analysis/feature_selection_with_boruta_human/")
make_recursive_dir(boruta_base_dir)

########## Local functions #########################################

run_boruta = function(jobid, df, var, num.threads = 1, mRun = 500) {

  require(Boruta)
  Boruta(as.formula(paste(var, "~ .")), df, doTrace = 2, maxRuns = mRun)

}

run_ranger = function(jobid, df, var, num.threads = 1) {

  require(Boruta)
  ranger(as.formula(paste(var, "~ .")), df)

}

submit_sbatchtools = function(data, y, dir, job_id, n_threads = 16,
                              base_dir = boruta_base_dir) {


  # Specify a directory in which batchtools should create a "working" directory to store all results etc
  jobDirectory = file.path(base_dir, dir)
  make_recursive_dir(jobDirectory)

  # Initialize batchtools for SLURM
  reg = initBatchtoolsSLURM(jobDirectory)

  reg = setDefaultResources(registry = reg,
                            maxTime = "10:00:00",
                            memory = 2000,
                            nCPUs = n_threads,
                            qos = "high")

  ids = batchMap(run_boruta, jobid = job_id, more.args = list(df = data, var = y, num.threads = n_threads))

  submitJobs(ids)
  ids

}

################################################################################
############# Pre-process and submit ###########################################
################################################################################

human_path = file.path(project_folder, "/analysis/human_data")
mt = read.csv(file.path(human_path, "variation_prior.csv"))

mt_lung = read.csv(file.path(human_path, "/mt_lung.csv"))
mt_ovary = read.csv(file.path(human_path, "/mt_ovary.csv"))
mt_muscle = read.csv(file.path(human_path, "/mt_muscle.csv"))


remove_list = names(mt)[grep("_cv|_median|variation|_mean|Prior_Rank|mean_level|gene_name|gene_id", names(mt))]
l0 = setdiff(remove_list, "mean_variation")
l1 = setdiff(remove_list, "mean_variation_glob")
l2 = setdiff(remove_list, "mean_level")
l3 = setdiff(remove_list, "DE_Prior_Rank")

# mean variation, mean level and DE prior
mt0 = mt %>% select(-one_of(l0)) %>% na.omit()
mt1 = mt %>% select(-one_of(l1)) %>% na.omit()
mt2 = mt %>% select(-one_of(l2)) %>% na.omit()
mt3 = mt %>% select(-one_of(l3)) %>% na.omit()

# tissue-specific data
mt4 = mt_lung %>% select(-median, -gene_id)
mt5 = mt_lung %>% select(-resid_cv, -gene_id)
mt6 = mt_ovary %>% select(-median, -gene_id)
mt7 = mt_ovary %>% select(-resid_cv, -gene_id)
mt8 = mt_muscle %>% select(-median, -gene_id)
mt9 = mt_muscle %>% select(-resid_cv, -gene_id)

#################### Submit ###################

id0 = submit_sbatchtools (data = mt0, y = "mean_variation", "predict_mean_variation", job_id = "j0", n_threads = 16)
id1 = submit_sbatchtools (data = mt1, y = "mean_variation_glob", "predict_mean_variation_glob", job_id = "j1", n_threads = 16)
id2 = submit_sbatchtools (data = mt2, y = "mean_level", "predict_mean_level", job_id = "j2", n_threads = 16)
id3 = submit_sbatchtools (data = mt3, y = "DE_Prior_Rank", "DE_Prior_Rank", job_id = "j3", n_threads = 16)

# id4 = submit_sbatchtools (data = mt4, y = "resid_cv", "predict_resid_cv_lung", job_id = "j4", n_threads = 16)
# id5 = submit_sbatchtools (data = mt5, y = "median", "predict_median_lung", job_id = "j5", n_threads = 16)
# id6 = submit_sbatchtools (data = mt6, y = "resid_cv", "predict_resid_cv_ovary", job_id = "j6", n_threads = 16)
# id7 = submit_sbatchtools (data = mt7, y = "median", "predict_median_ovary", job_id = "j7", n_threads = 16)
# id8 = submit_sbatchtools (data = mt8, y = "resid_cv", "predict_resid_cv_muscle", job_id = "j8", n_threads = 16)
# id9 = submit_sbatchtools (data = mt9, y = "median", "predict_median_muscle", job_id = "j9", n_threads = 16)

