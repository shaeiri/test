
## Essential libraries.
library(plyr)
library(dplyr)


## Reading tables.
loc0 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/making_table/which/which.csv"
which <- read.csv(loc0, header = TRUE, sep = ",")
table <- which


loc1 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/target/results/all_variations3.csv"
target <- read.csv(loc1, header = TRUE, sep = ",")
table <- join(table, target, by = "gene_id", type = "inner")
table$sd_variation <- NULL


loc2 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/genes/results/genes.csv"
genes <- read.csv(loc2, header = TRUE, sep = ",")
names(genes)[1] <- "gene_id"
table <- join(genes, table, by = "gene_id", type = "inner")


loc3 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/exons/results/exons.csv"
exons <- read.csv(loc3, header = TRUE, sep = ",")
names(exons)[1] <- "gene_id"
table <- join(exons, table, by = "gene_id", type = "inner")


loc4 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/transcripts/results/tr_number.csv"
tr_number <- read.csv(loc4, header = TRUE, sep = ",")
table <- join(tr_number, table, by = "gene_id", type = "inner")


loc5 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/transcripts/results/transcripts.csv"
transcripts <- read.csv(loc5, header = TRUE, sep = ",")
table <- join(table, transcripts, by = "TXNAME", type = "inner")


loc6 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/gccontents/results/gccontents.csv"
gccontents <- read.csv(loc6, header = TRUE, sep = ",")
table <- join(table, gccontents, by = "TXNAME", type = "inner")


loc7 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/CpGislands/results/CpGislands.csv"
CpGislands <- read.csv(loc7, header = TRUE, sep = ",")
table <- join(table, CpGislands, by = "TXNAME", type = "inner")


loc8 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/TFs-motifs/results/tata.csv"
tata <- read.csv(loc8, header = TRUE, sep = ",")
table <- join(table, tata, by = "TXNAME", type = "inner")


loc9 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/TFs-motifs/results/TFs2.csv"
TFs <- read.csv(loc9, header = TRUE, sep = ",")
table <- join(table, TFs, by = "TXNAME", type = "inner")


loc11 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/promoter_architecture/results/share/share_shape.csv"
share_shape <- read.csv(loc11, header = TRUE, sep = ",")
table <- join(table, share_shape, by = "TXNAME", type = "inner")
table$share_broad <- NULL
table$median_si <- NULL
table$median_width <- NULL


loc13 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/share_states"
share_state <- read.csv(loc13, header = TRUE, sep = ",")
table <- join(table, share_state, by = "TXNAME", type = "inner")
table$median_BivFlnk <- NULL
table$median_Enh <- NULL
table$median_EnhBiv <- NULL
table$median_EnhG <- NULL
table$median_Het <- NULL
table$median_Quies <- NULL
table$median_ReprPC <- NULL
table$median_ReprPCWk <- NULL
table$median_TssA <- NULL
table$median_TssAFlnk <- NULL
table$median_TssBiv <- NULL
table$median_Tx <- NULL
table$median_TxFlnk <- NULL
table$median_TxWk <- NULL
table$median_ZNF_Rpts <- NULL


loc14 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/share_histones1"
share_histones <- read.csv(loc14, header = TRUE, sep = ",")
table <- join(table, share_histones, by = "TXNAME", type = "inner")
table$median_H3K4me1 <- NULL
table$median_H3K9me3 <- NULL
table$median_H3K27ac <- NULL
table$median_H3K9ac <- NULL
table$median_H3K27me3 <- NULL
table$median_H3K36me3 <- NULL
table$median_H3K4me3 <- NULL


## Computing mean variation.
table <- cbind(rownames(table), table)
names(table)[1] <- "row_num"

span1 <- 0.25
degree1 <- 1
for (i in seq(9, 180, 4)){
  table$row_num <- rownames(table)
  
  model1 <- loess(as.formula(paste(colnames(table)[i + 2], "~", colnames(table)[i + 1]))
                  , data = table, degree = degree1, span = span1)
  r1 <- resid(model1)
  r1 <- as.data.frame(r1)
  r1$row_num <- rownames(r1)
  names(r1)[1] <- gsub("mean", "resid_cv", colnames(table)[i])
  
  table <- merge(table, r1, by = "row_num", all = TRUE)
  
  print(paste(colnames(table)[i + 2], "~", colnames(table)[i + 1]))
}

matrix <- as.matrix(table[, seq(660, 702)])
table$mean_variation <- rowMeans(matrix, na.rm = TRUE)


# It is not complete!
# model <- loess(mean_cv ~ mean_median, data = table, degree = degree1, span = span1)
# r1 <- resid(model)
# names(r1) = "mean_variation_global"
# r1$row_num <- rownames(r1)
# table$row_num <- rownames(table)
# table <- merge(table, r1, by = "row_num", all = TRUE)


## Adding new features.
list1 <- c()
list2 <- c()
for (i in seq(191, 634)) {
  
  sd <- sd(table[, i])
  if (sd == 0 | is.na(sd))
    next
  
  value <- cor(table$mean_variation, table[, i])
  
  if (value > 0)
    list1 <- c(list1, names(table)[i])
  else
    list2 <- c(list2, names(table)[i])
}

matrix1 <- as.matrix(table[, list1])
sum1 <- rowSums(matrix1)

matrix2 <- as.matrix(table[, list2])
sum2 <- rowSums(matrix2)

table$pos_tfs <- sum1
table$neg_tfs <- sum2


## Editing features!
for (i in 660:702) {
  table[, 660] <- NULL
}

for (i in 9:180) {
  table[, 9] <- NULL
}

table$row_num <- NULL
table$TXNAME <- NULL


names(table)[names(table) == "number_tr"] <- "number of transcripts"
names(table)[names(table) == "mean_exons"] <- "mean exon length"
names(table)[names(table) == "dispersion_exons"] <- "exon length mean absolute deviation"
names(table)[names(table) == "number_exons"] <- "number of exons"
names(table)[names(table) == "length_genes"] <- "gene length"
names(table)[names(table) == "width_transcript"] <- "transcript width"
names(table)[names(table) == "GCcontent"] <- "GC-content"
names(table)[names(table) == "length"] <- "CpG length"
names(table)[names(table) == "cpgNum"] <- "CpG num"
names(table)[names(table) == "gcNum"] <- "GC num"
names(table)[names(table) == "TATA"] <- "TATA-box"
names(table)[names(table) == "percent_broad"] <- "percent of broad"
names(table)[names(table) == "mean_width"] <- "mean promoter width"

names(table)[names(table) == "mean_TssA"] <- "mean TssA"
names(table)[names(table) == "mean_TssAFlnk"] <- "mean TssAFlnk"
names(table)[names(table) == "mean_TxFlnk"] <- "mean TxFlnk"
names(table)[names(table) == "mean_Tx"] <- "mean Tx"
names(table)[names(table) == "mean_TxWk"] <- "mean TxWk"
names(table)[names(table) == "mean_EnhG"] <- "mean EnhG"
names(table)[names(table) == "mean_Enh"] <- "mean Enh"
names(table)[names(table) == "mean_ZNF_Rpts"] <- "mean ZNF_Rpts"
names(table)[names(table) == "mean_Het"] <- "mean Het"
names(table)[names(table) == "mean_TssBiv"] <- "mean TssBiv"
names(table)[names(table) == "mean_BivFlnk"] <- "mean BivFlnk"
names(table)[names(table) == "mean_EnhBiv"] <- "mean EnhBiv"
names(table)[names(table) == "mean_ReprPC"] <- "mean ReprPC"
names(table)[names(table) == "mean_ReprPCWk"] <- "mean ReprPCWk"
names(table)[names(table) == "mean_Quies"] <- "mean Quies"


table$mean_si <- NULL

table$H3K4me3 <- NULL
table$H3K4me1 <- NULL
table$H3K9me3 <- NULL
table$H3K9ac <- NULL
table$H3K27me3 <- NULL
table$H3K27ac <- NULL
table$H3K36me3 <- NULL

table$mean_H3K4me1 <- NULL
table$mean_H3K4me3 <- NULL
table$mean_H3K9me3 <- NULL
table$mean_H3K9ac <- NULL
table$mean_H3K27me3 <- NULL
table$mean_H3K27ac <- NULL
table$mean_H3K36me3 <- NULL

table$number_TFs <- table$pos_tfs + table$neg_tfs
table$pos_tfs <- NULL
table$neg_tfs <- NULL
names(table)[names(table) == "number_TFs"] <- "number of TFs"


# table$gene_id <- NULL
table$mean_cv <- NULL
# names(table)[names(table) == "mean_median"] <- "mean level"
# names(table)[names(table) == "mean_variation"] <- "mean variation"


names(table) = gsub("-", "_", names(table))
names(table) = gsub(" ", "_", names(table))


## Saving.
location <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/final/share.csv"
write.csv(table, location, row.names = FALSE)


## Reading Prior.
loc4 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/analysis3/data/DE_Prior.txt"
priors <- read.csv(loc4, header = TRUE, sep = "\t")


## Translation and Joinning.
ensembl = useEnsembl(biomart = "ensembl", dataset = "hsapiens_gene_ensembl")
hgnc_swissprot <- getBM(attributes=c('ensembl_gene_id', 'hgnc_symbol'),
                        filters = 'ensembl_gene_id',
                        values = table$gene_id, mart = ensembl)
names(hgnc_swissprot)[1] <- "gene_id"
names(hgnc_swissprot)[2] <- "Gene_Name"
hgnc_swissprot <- hgnc_swissprot[hgnc_swissprot$Gene_Name != "" , ]

t <- join(table, hgnc_swissprot, by = "gene_id", type = "inner")
t <- join(t, priors, by = "Gene_Name", type = "inner")

t$Gene_Order <- NULL
t$Gene_EntrezID <- NULL
t$N_HitLists <- NULL
# t$Gene_Name <- NULL


## Saving.
location <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/final/prior.csv"
write.csv(t, location, row.names = FALSE)


## Reading genes.
loc100 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/analysis2/data/data/"
names1 <- list.files(path = loc100, full.names = TRUE)
names2 <- list.files(path = loc100, full.names = FALSE)

table <- t

for (i in seq(length(names1))) {
  
  temp <- read.csv(names1[i], header = FALSE, sep = "\t")
  names(temp)[1] <- "Gene_Name"
  temp$temp <- "YES"
  names(temp)[2] <- names2[i]
  
  table <- join(table, temp, "Gene_Name", type = "left")
  table[, names2[i]][is.na(table[, names2[i]])] <- "NO"
  table[, names2[i]] <- as.factor(table[, names2[i]])
}


## Saving.
location <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/final/disease.csv"
write.csv(table, location, row.names = FALSE)
